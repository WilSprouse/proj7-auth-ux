"""
Flask-Login example
===================
This is a small application that provides a trivial demonstration of
Flask-Login, including remember me functionality.

:copyright: (C) 2011 by Matthew Frazier.
:license: MIT/X11, see LICENSE for more details.
:source: https://gist.github.com/robbintt/32074560e52e46788237
"""
#from flask_wtf import FlaskForm
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required) #, LoginForm)
from flask import Flask, jsonify, session, abort, Response, request, render_template, redirect, url_for, flash
from flask_wtf import FlaskForm
from wtforms import Form, BooleanField, StringField, PasswordField, SubmitField, validators
import uuid
from pymongo import MongoClient
from passlib.apps import custom_app_context
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import json
from flask_restful import Resource, Api
from bson.json_util import loads, dumps
import pandas

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#client = MongoClient("172.21.0.2", 27017)

db = client.userdb

def myOpen(e):
    return e['open']

def myClose(e):
    return e['close']

class LoginForm(Form):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')
    remember = BooleanField('Remember Me')

class RegisterForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    password = PasswordField("Password", [validators.Length(min=4, max=25)])
    submit = SubmitField('Submit')

# your user class 
class User(UserMixin):
    def __init__(self, name, password, active=True):
        self.username = name
        self.password = password
        self.id = str(uuid.uuid4())
        #session['id'] = self.id
        self.active = active

    def is_active(self):
        return self.active

    def save(self):
        return {
                'Username': self.username,
                'Password': self.password,
                'id': self.id
                }

def hash_password(password):
    return custom_app_context.hash(password)

def verify_password(password, hashedPass):
    #app.logger.debug(hash_password(password))
    #app.logger.debug(hashedPass)
    #return hash_password(password) == hashedPass
    return custom_app_context.verify(password, hashedPass)

SECRET_KEY = "yeah, not actually a secret"
DEBUG = True

def generate_auth_token(expiration, ids):
    s = Serializer(SECRET_KEY, expires_in=expiration)
    app.logger.debug(ids)
    token = s.dumps({'id':ids}) # Needs to be getID of user
    return token
    #return s.dumps({ 'token':token, 'duration': expiration })


def verify_auth_token(token):
    s = Serializer(SECRET_KEY)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return False    # valid token, but expired
    except BadSignature:
        return False    # invalid token
    return True

# note that the ID returned must be unicode
USERS = {
    1: User(u"A", 1),
    2: User(u"B", 2),
}

USER_NAMES = dict((u.username, u) for u in USERS.values()) #itervalues())

app = Flask(__name__)
api = Api(app)


app.config.from_object(__name__)

# step 1 in slides
login_manager = LoginManager()

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"


#login_manager.setup_app(app)
# step 2 in slides
@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

login_manager.setup_app(app)

@app.route("/<user>")
def hello(user):
    #return render_template("hello.html",name=user)
    return Response(status=201)


@app.route("/index")
def index():
    return render_template("index.html")

class AllTimes(Resource):
    def get(self):
        db = client.userdb
        for i in db.userdb.find():
            if i['Username'] == session['username']:
                token = generate_auth_token(30, i['id']) #, 30)
                if verify_auth_token(token):
                    app.logger.debug("Confirmed Token")
                    db = client.tododb
                    return json.loads(dumps(db.tododb.find()))
        return abort(401)

class OpenTimes(Resource):
    def get(self):
        ret = []
        db = client.tododb
        app.logger.debug(json.loads(dumps(db.tododb.find({}, {"open":1}))))
        for i in json.loads(dumps(db.tododb.find({}, {"open":1}))):
            if len(i) != 1:
                app.logger.debug(i)
                ret.append(i)
        return json.loads(dumps(ret))

class CloseTimes(Resource):
    def get(self):
        ret = []
        db = client.tododb
        for i in json.loads(dumps(db.tododb.find({}, {"close":1}))):
            if len(i) != 1:
                app.logger.debug(i)
                ret.append(i)

        return json.loads(dumps(ret))
        
class AllTimesJSON(Resource):
    def get(self):
        db = client.tododb
        return json.loads(dumps(db.tododb.find()))


class OpenTimesJSON(Resource):
    def get(self):
        ret = []
        db = client.tododb
        topK = request.args.get("top")
        app.logger.debug(topK)
        for i in json.loads(dumps(db.tododb.find({}, {"open":1}))):
            if len(i) != 1:
                if '_id' in i:
                    del i['_id']
                app.logger.debug(i)
                ret.append(i)
                ret.sort(key=myOpen)
        if topK != None:
            cnt = 0
            topKList = []
            for i in ret: #json.loads(dumps(db.tododb.find({}, {"open":1}))):
                app.logger.debug(i)
                if '_id' in i:
                    del i['_id']
                if len(i) != 0:
                    cnt += 1
                    topKList.append(i)
                    #sortedRet = sortThis(ret)
                    if cnt == int(topK):
                        return topKList

        return json.loads(dumps(ret))


class CloseTimesJSON(Resource):
    def get(self):
        ret = []
        db = client.tododb
        topK = request.args.get("top")
        app.logger.debug(topK)
        for i in json.loads(dumps(db.tododb.find({}, {"close":1}))):
            if len(i) != 1:
                if '_id' in i:
                    del i['_id']
                app.logger.debug(i)
                ret.append(i)
                ret.sort(key=myClose)
        if topK != None:
            cnt = 0
            topKList = []
            for i in ret: #json.loads(dumps(db.tododb.find({}, {"close":1}))):
                app.logger.debug(i)
                if '_id' in i:
                    del i['_id']
                if len(i) != 0:
                    cnt += 1
                    topKList.append(i)
                    if cnt == int(topK):
                        return topKList
        return json.loads(dumps(ret))

"""Getting the CSV formats"""
class AllTimesCSV(Resource):
    def get(self):
        db = client.tododb
        data = db.tododb.find()
        df = pandas.DataFrame(json.loads(dumps(data)))
        return df.to_csv() # Might have to specify file


class OpenTimesCSV(Resource):
    def get(self):
        ret = []
        db = client.tododb
        topK = request.args.get("top")
        app.logger.debug(topK)
        for i in json.loads(dumps(db.tododb.find({}, {"open":1}))):
            if len(i) != 1:
                if '_id' in i:
                    del i['_id']
                app.logger.debug(i)
                ret.append(i)
                ret.sort(key=myOpen)
        if topK != None:
            cnt = 0
            topKList = []
            for i in ret: #json.loads(dumps(db.tododb.find({}, {"open":1}))):
                app.logger.debug(i)
                if '_id' in i:
                    del i['_id']
                if len(i) != 0:
                    cnt += 1
                    topKList.append(i)

                    if cnt == int(topK):
                        return pandas.DataFrame(json.loads(dumps(topKList))).to_csv()

        return pandas.DataFrame(json.loads(dumps(ret))).to_csv()

class CloseTimesCSV(Resource):
    def get(self):
        ret = []
        db = client.tododb
        topK = request.args.get("top")
        app.logger.debug(topK)
        for i in json.loads(dumps(db.tododb.find({}, {"close":1}))):
            if len(i) != 1:
                if '_id' in i:
                    del i['_id']
                app.logger.debug(i)
                ret.append(i)
                ret.sort(key=myClose)
        if topK != None:
            cnt = 0
            topKList = []
            for i in ret: #json.loads(dumps(db.tododb.find({}, {"close":1}))):
                app.logger.debug(i)
                if '_id' in i:
                    del i['_id']
                if len(i) != 0:
                    cnt += 1
                    topKList.append(i)
                    if cnt == int(topK):
                        return pandas.DataFrame(json.loads(dumps(topKList))).to_csv()

        return pandas.DataFrame(json.loads(dumps(ret))).to_csv()





@app.route("/api/register", methods=["GET", "POST"])
def register():
    form = RegisterForm(request.form)

    if request.method == 'POST':
        if form.validate():
            user = User(form.username.data, hash_password(form.password.data))
            db.userdb.insert_one(user.save())
            app.logger.debug(user.save())
            return redirect(url_for('hello',user=form.username.data))
        else:
            return abort(400)

    return render_template("register.html", form=form)


@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")


@app.route("/apitoken")
def apitoken():
    for i in db.userdb.find():
        if i['Username'] == session['username']:
            #app.logger.debug(i['id'])
            s = Serializer(SECRET_KEY, expires_in=30)
            app.logger.debug(type(i['id']))
            token = s.dumps({'id':i['id']}) # Needs to be getID of user
            app.logger.debug(s.loads(token))
            if verify_auth_token(token):
                return str({'token': token, 'duration': 30})
            else:
                return abort(401)
    return render_template("test.html")


#@app.route("/api/token", methods=["GET","POST"])
@app.route("/api/token")
@login_required
def token():
    return render_template("Test.html") #pass # Do something here

# step 3 in slides
# This is one way. Using WTForms is another way.
@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)
    if request.method == "POST" and "username" in request.form:
        username = form.username.data #request.form["username"]
        userExists = False
        #app.logger.debug("JUICED")
        for i in db.userdb.find():
            if i['Username'] == username:
                #userExists = True
                #app.logger.debug(i) #db.userdb.find({}, {"Username":str(username)}))
                if verify_password(form.password.data, i['Password']):
                    #app.logger.debug("JUICY")
                    next_page = request.args.get('next')
                    #app.logger.debug("JUICY")
                    #app.logger.debug(next_page)
                    flash("Logged in!")
                    session['username'] = username
                    if (next_page != None):
                        #app.logger.debug("JUICY")
                        #app.logger.debug(next_page)
                        page = ''
                        next_page = next_page.split('/')#.join()
                        page = page.join(next_page)
                        app.logger.debug(page)

                        return redirect(page) #request.args.get("next"))# or url_for("index"))
                        #return redirect('apitoken')
                    return redirect(url_for('index'))
        '''
        if usernameExists: 
            #remember = request.form.get("remember", "no") == "yes"
            if login_user(USER_NAMES[username], remember=remember):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("index"))
            else:
                flash("Sorry, but you could not log in.")
            #else:
        '''
        flash(u"Invalid username.")

    return render_template("login.html", form=form, next=request.args.get('next'))

'''
@app.route("/api/token", methods=["GET","POST"])
@login_required
def token():
    return render_template("Test.html") #pass # Do something here
'''

# step 5 in slides
@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))


api.add_resource(AllTimes, '/listAll')
api.add_resource(OpenTimes, '/listOpenOnly')
api.add_resource(CloseTimes, '/listCloseOnly')
api.add_resource(AllTimesJSON, '/listAll/json')
api.add_resource(OpenTimesJSON, '/listOpenOnly/json')
api.add_resource(CloseTimesJSON, '/listCloseOnly/json')
api.add_resource(AllTimesCSV, '/listAll/csv')
api.add_resource(OpenTimesCSV, '/listOpenOnly/csv')
api.add_resource(CloseTimesCSV, '/listCloseOnly/csv')




if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
