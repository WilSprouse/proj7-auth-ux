* If you are a user *

This is a project to show basic authentication via username and password authentication, and another
implementation of token based authentiaction. Username and passwords are stored in a database
securely.

You can expose brevet time data in the database with urls:

"http://<host:5000>/listAll" returns all open and close times in the database
"http://<host:5000>/listOpenOnly" returns open times only
"http://<host:5000>/listCloseOnly" returns close times only

"http://<host:5000>/listAll/csv" returns all open and close times in CSV format

"http://<host:5000>/listOpenOnly/csv" returns open times only in CSV format
"http://<host:5000>/listCloseOnly/csv" returns close times only in CSV format
"http://<host:5000>/listAll/json" returns all open and close times in JSON format
"http://<host:5000>/listOpenOnly/json" returns open times only in JSON format
"http://<host:5000>/listCloseOnly/json" returns close times only in JSON format

"http://<host:5000>/listOpenOnly/csv?top=k" returns top k open times only (in ascending order) in CSV format

"http://<host:5000>/listOpenOnly/json?top=k" returns top k open times only (in ascending order) in JSON format
"http://<host:5000>/listCloseOnly/csv?top=k" returns top k close times only (in ascending order) in CSV format
"http://<host:5000>/listCloseOnly/json?top=k" returns top k close times only (in ascending order) in JSON format

You can register with the url "http://<host:5000>/api/register
You can get a token with the url "http://<host:5000>/api/token
-----------
Other notes:
- This is as much as I got done before I saw the message that our lowest project score will be dropped.
- Given this term's inherent challenges being at home, combined with the current climate of the U.S.
  I am hoping this is my lowest score, as I have recieved over 100% in the other projects combined.  

Thanks for a great term!


Author: Wil Sprouse
Email: wils@cs.uoregon.edu


